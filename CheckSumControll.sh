#!/bin/bash
#--------------------------------------------------------------------------------------
# Version 1.0, 03.09.2019
# Falko Schmidt, GERMANY
#--------------------------------------------------------------------------------------
#    ___ _               _                        
#   / __\ |__   ___  ___| | _____ _   _ _ __ ___  
#  / /  | '_ \ / _ \/ __| |/ / __| | | | '_ ` _ \ 
# / /___| | | |  __/ (__|   <\__ \ |_| | | | | | |
# \____/|_| |_|\___|\___|_|\_\___/\__,_|_| |_| |_|
#                                                
#--------------------------------------------------------------------------------------
# Dieses Skript wurde speziell für die Überprüfung einer heruntergelandenen
# Raspbian-ZIP-Datei erstellt, kann jedoch für die Überprüfung aller SHA256-Checksummen
# verwendet werden
# 
# Checksumme für Raspbian Buster Lite von 10.07.2019:
# 9e5cf24ce483bb96e7736ea75ca422e3560e7b455eee63dd28f66fa1825db70e
#
#--------------------------------------------------------------------------------------

# Präfix für die Ausgabe definieren
readonly PREFIX="[CheckSumController]"

# Farben definieren
readonly BLUE='\033[1;34m'
readonly GREEN='\033[1;32m'
readonly RED='\033[1;31m'
readonly NORMAL='\033[0;39m'

# Ausgabefunktion definieren
function print_msg() {
	echo -e "$BLUE ${PREFIX} $1 $NORMAL "
}

# Wenn nicht zwei Argumente angegeben sind, ist der Aufruf dieses
# Bash-Skripts fehlerhaft, das Programm wird beendet.
if [ $# != 2 ]; then
	print_msg "$RED Falsche Argumente, verwende ./CheckSumControll.sh <Dateipfad> <Checksumme>"
	exit
fi

# Prüfe, ob Datei unter gegebenem Pfad existiert
if [ ! -e "$1" ]; then
	print_msg "$RED Die gegebene Datei $1 existiert nicht!"
	exit
fi

# Information ausgeben
print_msg "$GREEN Berechne Cheksumme der Datei $1..."

# SHA256-Summe der gegebenen Datei berechnen und das Resultat an den
# Leerzeichen getrennt in einem Array speichern
IFS=' ' read -r -a cmdResult <<< "$( shasum -a 256 $1 )"

# Berechnete Checksumme ausgeben
print_msg " ${cmdResult[0]}"

# Berechnete Checksumme mit gegebener Checksumme vergleichen
if [ "$2" = "${cmdResult[0]}" ]; then
	print_msg "$GREEN Die Checksumme stimmt, die Datei wurde korrekt heruntergeladen!"
else
	print_msg "$RED Die Checksumme ist fehlerhaft, bitte lade die Datei erneut herunter!"
fi
