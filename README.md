# CheckSumControll

Letztes Update: 03.09.2019
Letzte Version: 1.0

Getestete OS: macOS 10.14.6

Dieses Bash-Skript prüft die SHA-256-Checksumme und kann unter dem Betriebssystem macOS
ausgeführt werden.

Die Ausführung des Skripts erfolgt gemäß folgender Schritte:

(1) Skript downloaden

(2) In einem Konsolenfenster zu dem Ordner mit der Bashdatei navigieren

    cd <Pfadname>

(3) Zugriffsrechte mit

    sudo chmod +x CheckSumControll.sh
    
um das Ausführungsrecht erweitern, sodass der Besitzer der Datei diese ausführen
kann.

(4) Dateipfad der zu prüfenden Datei sowie die korrekte Checksumme in das
Konsolenfenster kopieren, sodass der Aufruf insgesamt das Format

    ./CheckSumControll.sh <Dateipfad> <Checksumme>
    
besitzt.
Sollte das Programm die Checksumme als fehlerhaft kennzeichnen, empfiehlt es
sich, die geprüfte Datei erneut herunterzuladen.

Trotz großer Sorgfalt beim Programmieren sowie verschiedenen Testreihen sind
Fehler niemals auszuschließen. Deshalb erfolgt die Nutzung dieses Skripts auf
eigene Gefahr, es wird keine Haftung übernommen.

Viel Spaß bei der Nutzung

- [DEV] Falko Schmidt